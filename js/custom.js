(function ($) {
  jQuery(document).ready(function ($) {
    $('.full-page-search .search-icon a').click(function (e) {
      e.preventDefault(); // Prevent default link behavior
      $('.full-page-search .search-box').fadeIn(); // Show the search box
    });
    $('.full-page-search .search-box-close').click(function () {
      $('.full-page-search .search-box').fadeOut(); // Hide the search box
    });
  });


  jQuery(document).ready(function ($) {
    // Mobile menu.
    $('.mobile-menu').click(function () {
      $(this).next('.primary-menu-wrapper').toggleClass('active-menu');
    });
    $('.close-mobile-menu').click(function () {
      $(this).closest('.primary-menu-wrapper').toggleClass('active-menu');
    });
  });

  Drupal.behaviors.mobileMenuToggle = {
    attach: function (context, settings) {
      document.querySelector('.navbar-toggle').addEventListener('click', function () {
        document.querySelector('.navbar-collapse').classList.toggle('show');
      });
    }
  }

  Drupal.behaviors.cursor = {
    attach: function (context, settings) {
      const cursor = document.querySelector('.cursor');

      document.addEventListener('mousemove', e => {
        cursor.setAttribute("style", "top: " + (e.pageY - 10) + "px; left: " + (e.pageX - 10) + "px;")
      });

      document.addEventListener('click', e => {
        cursor.classList.add("expand");
        setTimeout(() => {
          cursor.classList.remove("expand");
        }, 500);
        console.log('cursor');
      });
    }
  }

  var scroll = $('.scrolltop');
  $(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
      scroll.addClass('show');
    } else {
      scroll.removeClass('show');
    }
  });

  scroll.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
  });

})(jQuery);
